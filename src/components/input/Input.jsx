import React from "react";
import classes from "./input.module.css";


import { saveToLocalStorage } from "./../../localstorage";


function keyUpHandler(props, event, id) {
    let state = props.state,
        setState = props.setState;

    if (event.keyCode !== 13) {

        setState({
            tasks: state.tasks,
            text: event.target.value,
        });

    } else if (state.text !== "") {
        let mas = state.tasks;

        let task = {
            text: state.text,
            check: false,
            id: mas.length !== 0 ? Math.max(...(mas.map((current) => current.id))) + 1 : 0,
        };

        mas.unshift(task);

        setState({
            tasks: mas,
            text: event.target.value,
        });

        // saveToLocalStorage(id, mas);

        if (props.isActive.All === true) {
            props.setCount(mas.length);
        }
        if (props.isActive.Active === true) {
            let count = mas.reduce((count, current) => count + !current.check, 0);
            props.setCount(count);
        }
        if (props.isActive.Completed === true) {
            let count = mas.reduce((count, current) => count + !!current.check, 0);
            props.setCount(count);
        }

        event.target.value = "";
    }
}

const Input = (props) => {
    return (
        <div className={classes.inputContainer}>
            <input type="text" className={classes.input} placeholder="Enter task" onKeyUp={ (event) => { keyUpHandler(props, event, props.id) } } />
        </div>
    )
}

export default Input;