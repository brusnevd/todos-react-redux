import React from 'react';
import classes from "./tasks.module.css"
import { saveToLocalStorage } from "./../../localstorage";

function deleteTask(event, props) {
    let mas = props.state.tasks.filter((current) => {
        return "del" + current.id !== event.target.id;
    });

    // saveToLocalStorage(props.id, mas);

    props.setState(
        {
            tasks: mas,
            text: "",
        }
    );

    if (props.isActive.All === true) {
        props.setCount(mas.length);
    }
    if (props.isActive.Active === true) {
        let count = mas.reduce((count, current) => count + !current.check, 0);
        props.setCount(count);
    }
    if (props.isActive.Completed === true) {
        let count = mas.reduce((count, current) => count + !!current.check, 0);
        props.setCount(count);
    }

    // saveToLocalStorage(props.id, props.state);

    // props.setCount(props.count - 1);
}

function checkboxChange(event, props) {
    let mas = props.state.tasks.map((current) => {
        if ("checkbox" + current.id === event.target.id) {
            if (current.check === false) {
                current.check = true;
            } else {
                current.check = false;
            }
        }

        return current;
    });

    // saveToLocalStorage(props.id, mas);

    props.setState(
        {
            tasks: mas,
            text: "",
        }
    );

    //saveToLocalStorage(props.id, props.state);

    if (props.isActive.Active === true) {
        let count = props.state.tasks.reduce((count, current) => count + !current.check, 0);
        props.setCount(count);
    }
    if (props.isActive.Completed === true) {
        let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);
        props.setCount(count);
    }
}

const Tasks = (props) => {
    let highClass = false;
    let tasksMas = props.state.tasks.map((current) => {
        if (props.isActive.All === true) {
            highClass = true;
        }
        if (props.isActive.Active === true) {
            if (current.check) {
                highClass = false;
            } else {
                highClass = true;
            }
        }
        if (props.isActive.Completed === true) {
            if (current.check) {
                highClass = true;
            } else {
                highClass = false;
            }
        }
        return (
            <div key={current.id} className={classes.task + (highClass === true ? " " + classes.high : "")} >
                <input type="checkbox" id={"checkbox" + current.id} className={classes.checkbox} defaultChecked={current.check} onClick={(event) => checkboxChange(event, props)} />
                <label htmlFor={"checkbox" + current.id} className={classes.taskText + (current.check ? " " + classes.through : "")} onClick={(event) => checkboxChange(event, props)} > {current.text} </label>
                <span className={classes.delete} onClick={(event) => deleteTask(event, props)} id={"del" + current.id}>&#10006;</span>
            </div>
        )});

    return (
        <div className={classes.tasksContainer}>
            {tasksMas}
        </div>
    )
}

export default Tasks;