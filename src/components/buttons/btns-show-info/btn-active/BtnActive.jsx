import React from 'react';

function activeClickHandler(props) {
    if (props.isActive.Active) {
        props.setActive({
            All: false,
            Active: false,
            Completed: false,
        })
        props.setCount(0);
        props.setBtn("0");
    } else {
        props.setActive({
            All: false,
            Active: true,
            Completed: false,
        })
        let count = props.state.tasks.reduce((count, current) => count + !current.check, 0);
        props.setCount(count);
        props.setBtn("Active");
    }
}

const BtnActive = (props) => {
    return (
        <div onClick={() => activeClickHandler(props)}>
            Active
        </div>
    )
}

export default BtnActive;


// let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);