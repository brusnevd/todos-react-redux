import React from 'react';

function completedClickHandler(props) {
    if (props.isActive.Completed) {
        props.setActive({
            All: false,
            Active: false,
            Completed: false,
        })
        props.setCount(0);
        props.setBtn("0");
    } else {
        props.setActive({
            All: false,
            Active: false,
            Completed: true,
        })
        let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);
        props.setCount(count);
        props.setBtn("Completed");
    }
}

const BtnCompleted = (props) => {
    return (
        <div onClick={() => completedClickHandler(props)}>
            Completed
        </div>
    )
}

export default BtnCompleted;