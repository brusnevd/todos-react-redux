import React, { useState, useEffect } from 'react';
import Input from "./components/input/Input.jsx";
import Tasks from "./components/tasks/Tasks.jsx";
import Buttons from "./components/buttons/buttons.jsx";
import { saveToLocalStorage } from "./localstorage";

function App(props) {
  let id = props.config.id;

  let [state, setState] = useState({ tasks: [], text: "" });
  let [count, setCount] = useState(0);
  let [activeBtn, setBtn] = useState("0");
  let [isActive, setActive] = useState({
    All: false,
    Active: false,
    Completed: false,
  });

  useEffect(() => {
    if (localStorage.getItem("list" + id)) {
      console.log(localStorage.getItem("list" + id));
      setState({
        tasks: JSON.parse(localStorage.getItem("list" + id)),
        text: "",
      });
    }
  }, []);

  useEffect(() => {
    saveToLocalStorage(id, state.tasks);
  });

  return (
    <div className="container">
      <Input state={state} setState={setState} setCount={setCount} count={count} id={id} isActive={isActive} setActive={setActive} />
      <Tasks state={state} setState={setState} setCount={setCount} count={count} id={id} isActive={isActive} setActive={setActive} />
      <Buttons state={state} setState={setState} count={count} setCount={setCount} activeBtn={activeBtn} setBtn={setBtn} id={id} isActive={isActive} setActive={setActive} />
    </div>
  );
}

export default App;