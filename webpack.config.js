var path = require('path');
var HTMLWebpackPlugin = require("html-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");
var autoprefixer = require('autoprefixer');

module.exports = {
    entry: "./src/index.jsx", // входная точка - исходный файл
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    },
    module: {
        rules: [{
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "@babel/preset-env",
                            "@babel/preset-react",
                        ],
                        sourceMap: true,
                    }
                }
            },
        ],
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: "index.html"
        }),
        new CopyWebpackPlugin({
            patterns: [{
                    from: path.resolve(__dirname, "public/back.jpg"),
                    to: path.resolve(__dirname, "dist")
                },
                {
                    from: path.resolve(__dirname, "public/18338.otf"),
                    to: path.resolve(__dirname, "dist")
                }
            ]
        })
    ]
}